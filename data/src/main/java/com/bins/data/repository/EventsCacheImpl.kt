package com.bins.data.repository

import com.bins.data.db.EventsDao
import com.bins.data.db.EventsDatabase
import com.bins.data.mappers.DbEntityToDataEntityMapper
import com.bins.data.mappers.EntityToDataMapper
import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.reactive.openSubscription

class EventsCacheImpl(private val database: EventsDatabase,
                      private val entityToDataMapper: EntityToDataMapper,
                      private val dbToDataMapper: DbEntityToDataEntityMapper) : EventsDataStore {

    private val dao: EventsDao = database.getEventsDao()

    override suspend fun getEvents(): ReceiveChannel<DataEntity<List<EventsEntity>>> {
        val mappedValue = dao.getEvents().map {
            DataEntity.SUCCESS(dbToDataMapper.mapDbToDomainData(it))
        }
        return mappedValue.openSubscription()
    }

    fun saveUsers(response: DataEntity<List<EventsEntity>>) {
        entityToDataMapper.mapResponseToData(response)?.let {
            dao.deleleAll()
            dao.saveEvents(it)
        }
    }

}