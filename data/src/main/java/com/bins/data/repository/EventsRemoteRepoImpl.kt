package com.bins.data.repository

import com.bins.data.api.EventsApi
import com.bins.data.entities.EventsResponse
import com.bins.data.mappers.ResDataEntityMapper
import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.ErrorEntity
import com.bins.domain.entity.EventsEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import java.lang.Exception

class EventsRemoteRepoImpl constructor(private val api: EventsApi) : EventsDataStore {

    private val responseToEntityMapper = ResDataEntityMapper()

    override suspend fun getEvents(): ReceiveChannel<DataEntity<List<EventsEntity>>> {

        return GlobalScope.produce {
            try {
                val remoteEventslist :List<EventsResponse> = api.getEvents().await()
                send(DataEntity.SUCCESS(responseToEntityMapper.mapToEntity(remoteEventslist)))
            } catch (e: Exception) {
                send(DataEntity.ERROR(ErrorEntity(e.message)))
            }
        }
    }

}