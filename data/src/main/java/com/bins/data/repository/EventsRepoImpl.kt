package com.bins.data.repository

import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import com.bins.domain.repo.EventsRepo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.launch

class EventsRepoImpl(private val remote: EventsRemoteRepoImpl,
                     private val cache: EventsCacheImpl) : EventsRepo {

    override suspend fun getEvents(): ReceiveChannel<DataEntity<List<EventsEntity>>> {

        return GlobalScope.produce {

            launch {
              cache.getEvents().consumeEach { send(it) }
            }

            launch {
                when (val evnts = remote.getEvents().receive()) {
                    is DataEntity.SUCCESS -> {
                        cache.saveUsers(evnts)
                    }
                    is DataEntity.ERROR -> {
                        send(evnts)
                    }
                }
            }
        }
    }
}