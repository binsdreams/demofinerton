package com.bins.data.repository

import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import kotlinx.coroutines.channels.ReceiveChannel

interface EventsDataStore{
    suspend fun getEvents(): ReceiveChannel<DataEntity<List<EventsEntity>>>
}