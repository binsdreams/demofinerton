package com.bins.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bins.data.entities.EventsData

@Database(entities = [EventsData::class], version = 1)
abstract class EventsDatabase : RoomDatabase(){
    abstract fun getEventsDao() : EventsDao
}