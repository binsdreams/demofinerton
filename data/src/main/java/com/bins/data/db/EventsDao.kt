package com.bins.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bins.data.entities.EventsData
import io.reactivex.Flowable


@Dao
interface EventsDao {

    @Query("Select * from Events")
    fun getEvents(): Flowable<List<EventsData>?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveEvents(userDataList: List<EventsData>)

    @Query("DELETE FROM Events")
    fun deleleAll()
}
