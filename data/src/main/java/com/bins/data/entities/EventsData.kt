package com.bins.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*


@Entity(tableName = "Events")
data class EventsData(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    var companyName: String? = null,
    var lastUpdate: String? = null,
    var lastUpdatedInMinutes: String? = null,
    var group: String? = null)
