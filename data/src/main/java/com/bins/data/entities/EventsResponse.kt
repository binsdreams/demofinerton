package com.bins.data.entities

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class EventsResponse(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @SerializedName("companyName") var companyName: String? = null,
    @SerializedName("lastUpdate") var lastUpdate: String? = null,
    @SerializedName("lastUpdatedInMinutes") var lastUpdatedInMinutes: String? = null,
    @SerializedName("group") var group: String? = null
)