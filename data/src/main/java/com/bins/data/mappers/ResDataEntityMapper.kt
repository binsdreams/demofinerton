package com.bins.data.mappers

import com.bins.data.entities.EventsResponse
import com.bins.domain.entity.EventsEntity

class ResDataEntityMapper {

    fun mapToEntity(remoteEventsList:List<EventsResponse> ?) : List<EventsEntity>
            = remoteEventsList?.map { mapMapEventsResToEntity(it) } ?: emptyList()


    private fun mapMapEventsResToEntity(response: EventsResponse): EventsEntity = EventsEntity(
        id = response.id,
        companyName= response.companyName,
        lastUpdate= response.lastUpdate,
        lastUpdatedInMinutes=response.lastUpdatedInMinutes,
        group= response.group
    )
}