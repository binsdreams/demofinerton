package com.bins.data.mappers

import com.bins.data.entities.EventsData
import com.bins.domain.entity.EventsEntity

class DbEntityToDataEntityMapper {

    private fun mapDbToDataEntity(response: EventsData): EventsEntity = EventsEntity(
        id = response.id,
        companyName= response.companyName,
        lastUpdate= response.lastUpdate,
        lastUpdatedInMinutes=response.lastUpdatedInMinutes,
        group= response.group
    )


    fun mapDbToDomainData(mapEventsData: List<EventsData>?)  : List<EventsEntity> = mapEventsData?.map { mapDbToDataEntity(it) } ?: emptyList()

}