package com.bins.data.mappers

import com.bins.data.entities.EventsData
import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity

class EntityToDataMapper {

    private fun mapEventsToData(response: EventsEntity): EventsData = EventsData(
        companyName= response.companyName,
        lastUpdate= response.lastUpdate,
        lastUpdatedInMinutes=response.lastUpdatedInMinutes,
        group= response.group
    )

    fun mapResponseToData(response: DataEntity<List<EventsEntity>>): List<EventsData>? {
        return when (response) {
            is DataEntity.SUCCESS<List<EventsEntity>> ->
                response.data?.map { mapEventsToData(it) }
            is DataEntity.ERROR<List<EventsEntity>> ->
                response.data?.map { mapEventsToData(it) }
            is DataEntity.LOADING<List<EventsEntity>> ->
                response.data?.map { mapEventsToData(it) }
        }
    }
}