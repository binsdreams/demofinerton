package com.bins.data.api

import com.bins.data.entities.EventsResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface EventsApi {

    @GET("api/mobileapi/GetEventsLastUpdated")
    fun getEvents(): Deferred<List<EventsResponse>>

}