package com.bins.domain.entity

data class EventsEntity(
    var id: Int = 0,
    var companyName: String? = null,
    var lastUpdate: String? = null,
    var lastUpdatedInMinutes: String? = null,
    var group: String? = null)
