package com.bins.domain.repo

import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import kotlinx.coroutines.channels.ReceiveChannel

interface EventsRepo {

    suspend fun getEvents(): ReceiveChannel<DataEntity<List<EventsEntity>>>

}