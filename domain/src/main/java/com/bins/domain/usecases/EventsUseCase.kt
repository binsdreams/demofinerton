package com.bins.domain.usecases

import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import com.bins.domain.repo.EventsRepo
import kotlinx.coroutines.channels.ReceiveChannel
import kotlin.coroutines.CoroutineContext

class EventsUseCase(private val coroutineContext: CoroutineContext,
                    private val repositories: EventsRepo) : BaseUseCase<List<EventsEntity>>(coroutineContext) {

    override suspend fun getEventsData(data: Map<String, Any>?): ReceiveChannel<DataEntity<List<EventsEntity>>> {
        return repositories.getEvents()
    }

    override suspend fun sendToPresentation(data: DataEntity<List<EventsEntity>>): DataEntity<List<EventsEntity>> {
        return data
    }
}