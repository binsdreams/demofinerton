package com.sentry.modules.devices

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bins.finerton.R
import com.bins.finerton.databinding.EventsItemBinding
import com.bins.finerton.databinding.EventsTitleItemBinding
import com.bins.finerton.entity.Events

const val VIEW_TITLE = 1
const val VIEW_NORMAL = 0
const val NO_DIVIDER = 2
class EventsListAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var eventsList = mutableListOf<Events>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType){
            VIEW_TITLE ->{
                val eventsTitleItemBinding: EventsTitleItemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.events_title_item,
                    parent, false
                )
                return TitleViewHolder(eventsTitleItemBinding)
            }
            else ->{
                val eventsItemBinding: EventsItemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.events_item,
                    parent, false
                )
                return EventsViewHolder(eventsItemBinding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return eventsList[position].viewType
    }

    override fun getItemCount(): Int {
        return eventsList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is EventsViewHolder){
            holder.bind(eventsList[position])
        }else if(holder is TitleViewHolder){
            holder.bind(eventsList[position])
        }

    }

    class EventsViewHolder(private var binding: EventsItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(events: Events) {
            binding.root.tag = events.viewType
            var eventItemViewModel =EventsItemViewModel(events)
            binding.eventsVM =eventItemViewModel
        }
    }

    class TitleViewHolder(private var binding: EventsTitleItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(events: Events) {
            binding.root.tag = VIEW_TITLE
            var eventItemViewModel =EventsItemViewModel(events)
            binding.eventsVM =eventItemViewModel
        }
    }

    fun updateList(list: List<Events>) {
        eventsList.clear()
        eventsList.addAll(list)
        notifyDataSetChanged()
    }
}