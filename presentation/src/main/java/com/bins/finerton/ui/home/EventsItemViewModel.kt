package com.sentry.modules.devices

import androidx.databinding.BaseObservable
import com.bins.finerton.entity.Events
import java.text.SimpleDateFormat


class EventsItemViewModel constructor(private var events: Events) : BaseObservable() {

    fun getCompanyName(): String? {
        return events.companyName
    }

    fun getGroup(): String? {
        return events.group
    }

    fun getLastUpdated(): String? {
        return events.lastUpdate
    }

    fun getLastUpdatedInMinutes(): String? {
        return events.lastUpdatedInMinutes
    }
}
