package com.bins.finerton.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.bins.finerton.R
import com.bins.finerton.common.CustomDivider
import com.bins.finerton.entity.Data
import com.google.android.material.snackbar.Snackbar
import com.sentry.modules.devices.EventsListAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class EventsListFragment : Fragment() {

    private val homeViewModel: EventsListViewModel by viewModel()
    private var adapter: EventsListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity != null && !activity?.isFinishing!! && view != null && isAdded) {
            progress_circular.visibility = View.VISIBLE
            progressText.visibility = View.VISIBLE
            recycler_view.layoutManager = LinearLayoutManager(activity)
            adapter = EventsListAdapter()
            recycler_view.adapter = adapter
            var drable =context?.getDrawable(R.drawable.divider)
            val dividerItemDecoration: ItemDecoration = CustomDivider(drable )
                .padding(
                    resources.getDimensionPixelSize(R.dimen.divider_margin),
                    resources.getDimensionPixelSize(R.dimen.divider_margin)
                )
            recycler_view.addItemDecoration(dividerItemDecoration)
        }
        homeViewModel.fetchEventsList()
    }

    override fun onStart() {
        super.onStart()

        homeViewModel.getEventsList().observe(this, Observer {
            when (it) {
                is Data.ERROR -> {
                    progress_circular.visibility = View.GONE
                    progressText.visibility = View.GONE
                    var error = it.error.message as CharSequence
                    Snackbar.make(rootLayout,error,Snackbar.LENGTH_LONG).show()
                }
                is Data.LOADING -> {
                }
                is Data.SUCCESS -> {
                    progress_circular.visibility = View.GONE
                    progressText.visibility = View.GONE
                    it.data?.let { events -> adapter?.updateList(events) }
                }
            }
        })
    }
}