package com.bins.finerton.ui.home

import androidx.lifecycle.MutableLiveData
import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import com.bins.domain.usecases.EventsUseCase
import com.bins.finerton.base.BaseViewModel
import com.bins.finerton.base.Mapper
import com.bins.finerton.entity.Data
import com.bins.finerton.entity.Error
import com.bins.finerton.entity.Events
import com.sentry.modules.devices.NO_DIVIDER
import com.sentry.modules.devices.VIEW_TITLE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class EventsListViewModel(private val eventsUseCase: EventsUseCase,
                          private val mapper: Mapper<DataEntity<List<EventsEntity>>, Data<List<Events>>>) : BaseViewModel() {

    var dataList = MutableLiveData<Data<List<Events>>>()

    fun fetchEventsList() {
        launch {
            eventsUseCase.getEventsData().consumeEach { response ->
                val mappedResponse = group(mapper.mapFrom(response))

                withContext(Dispatchers.Main) {
                    dataList.postValue(mappedResponse)
                }
            }
        }
    }

    fun getEventsList() = dataList

    fun group(data: Data<List<Events>>): Data<List<Events>> {
        return when (data) {
            is Data.SUCCESS -> Data.SUCCESS(data.data?.let { crateGroupData(it) })
            is Data.ERROR -> Data.ERROR(error = Error( data.error.message))
            is Data.LOADING -> Data.LOADING()
        }
    }

    private fun crateGroupData(sources: List<Events>) :List<Events> {
        var groupMap = sources.let {
            it.groupBy { it ->
                it.group
            }
        }
        var groupedList = mutableListOf<Events>();
        for ((key, value) in groupMap) {
            groupedList.add(createdHeader(key))
            var lastItem = value.last()
            lastItem.viewType = NO_DIVIDER
            groupedList.addAll(value)
        }
        return groupedList
    }

    private fun createdHeader(name: String?): Events = Events(
        group= name,
        viewType = VIEW_TITLE
    )

}