package com.bins.finerton.entity

data class Events(
    var id: Int = 0,
    var companyName: String? = null,
    var lastUpdate: String? = null,
    var lastUpdatedInMinutes: String? = null,
    var group: String? = null,
    var viewType:Int = 0)
