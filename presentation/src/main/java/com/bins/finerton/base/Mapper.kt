package com.bins.finerton.base

abstract class Mapper<in T,E>{

    abstract fun mapFrom(from: T): E
}