package com.bins.finerton.di

import androidx.room.Room
import com.bins.data.api.EventsApi
import com.bins.data.db.EventsDatabase
import com.bins.data.mappers.DbEntityToDataEntityMapper
import com.bins.data.mappers.EntityToDataMapper
import com.bins.data.repository.EventsCacheImpl
import com.bins.data.repository.EventsRemoteRepoImpl
import com.bins.data.repository.EventsRepoImpl
import com.bins.domain.repo.EventsRepo
import com.bins.domain.usecases.EventsUseCase
import com.bins.finerton.mapper.EventEntityMapper
import com.bins.finerton.ui.home.EventsListViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

val mRepositoryModules = module {
    single(name = "remote") { EventsRemoteRepoImpl(api = get(EVENTS_API)) }
    single(name = "local") {
        EventsCacheImpl(
            database = get(DATABASE), entityToDataMapper = EntityToDataMapper(),
            dbToDataMapper = DbEntityToDataEntityMapper()
        )
    }
    single { EventsRepoImpl(remote = get("remote"), cache = get("local")) as EventsRepo }
}

val mUseCaseModules = module {
    factory(name = GET_EVENTS_USECASE) {
        EventsUseCase(
            coroutineContext = Dispatchers.Default,
            repositories = get()
        )
    }
}

val mNetworkModules = module {
    single(name = RETROFIT_INSTANCE) { createNetworkClient(BASE_URL) }
    single(name = EVENTS_API) { (get(RETROFIT_INSTANCE) as Retrofit).create(EventsApi::class.java) }
}

val mLocalModules = module {
    single(name = DATABASE) {
        Room.databaseBuilder(
            androidApplication(),
            EventsDatabase::class.java,
            "DemoFinerton"
        ).build()
    }
}

val mViewModels = module {

    viewModel {
        EventsListViewModel(eventsUseCase = get(GET_EVENTS_USECASE), mapper = EventEntityMapper())
    }

}



private const val BASE_URL = "http://fn-plt-pd-az-v2-rg01-asp-as-mobileapi.azurewebsites.net/"
private const val RETROFIT_INSTANCE = "Retrofit"
private const val EVENTS_API = "EVENTS_API"
private const val GET_EVENTS_USECASE = "GET_EVENTS_USECASE"
private const val DATABASE = "database"