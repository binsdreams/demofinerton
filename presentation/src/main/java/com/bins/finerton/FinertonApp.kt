package com.bins.finerton

import android.app.Application
import com.bins.finerton.di.*
import org.koin.android.ext.android.startKoin

class FinertonApp : Application() {

    override fun onCreate() {
        super.onCreate()
        loadKoin()
    }

    private fun loadKoin() {
        startKoin(this,
            listOf(
                mNetworkModules,
                mRepositoryModules,
                mUseCaseModules,
                mLocalModules,  mViewModels))
    }
}