package com.bins.finerton.mapper

import com.bins.finerton.base.Mapper
import com.bins.domain.entity.DataEntity
import com.bins.domain.entity.EventsEntity
import com.bins.finerton.entity.Data
import com.bins.finerton.entity.Error
import com.bins.finerton.entity.Events

class EventEntityMapper : Mapper<DataEntity<List<EventsEntity>>, Data<List<Events>>>() {

    override fun mapFrom(data: DataEntity<List<EventsEntity>>): Data<List<Events>> {
        return when (data) {
            is DataEntity.SUCCESS -> Data.SUCCESS(data.data?.let { mapSourcesToPresetation(it) })
            is DataEntity.ERROR -> Data.ERROR(error = Error( data.error.message))
            is DataEntity.LOADING -> Data.LOADING()
        }
    }

    private fun mapSourcesToPresetation(sources: List<EventsEntity>) :List<Events> = sources?.map { mapEventsDomainToPresentation(it) }


    private fun mapEventsDomainToPresentation(response: EventsEntity): Events = Events(
        companyName= response.companyName,
        lastUpdate= response.lastUpdate,
        lastUpdatedInMinutes=response.lastUpdatedInMinutes,
        group= response.group
    )

}